//При закрузке страницы  - показать на ней кнопку с текстом "Нарисовать круг". Данная кнопка должна быть единственным 
//контентом в теле HTML документа, весь остальной контент должен быть создан и добавлен на страницу с помощью JS.
//При нажатии на кнопку "Нарисовать круг" показывать одно поле ввода - диаметр круга. При нажатии на кнопку "нарисовать"
//создать на странице 100 кругов (10*10) случайного цвета. При клике на конкретный круг - этот круг должен исчезать,
//при этом пустое место заполняется, т.е. все круги сдвигаются влево.
let [btn] = document.getElementsByTagName("button");//
let diameter, div;
//створюємо функцію, що створює 100 div
function makeCircle() {
    for (let i = 1; i <= 100; i++) {
        document.body.prepend(document.createElement('div'));
    }
}
//запитуєм діаметр круга, передаєм його в змінну diameter
btn.onclick = function () {
    diameter = parseInt(prompt('Введіть діаметр круга'));
    //створюємо 100 div
    makeCircle();
    //створюємо масив з дивів і передаємо його в змінну div
    [...div] = document.getElementsByTagName('div');
    //додаємо стилі дивам
    div.forEach(function (item) {
        item.style.backgroundColor = `hsl(${Math.floor(Math.random() * 360)}, 100%, 50%)`;
        item.style.height = `${diameter}px`;
        item.style.width = `${diameter}px`;
        item.style.borderRadius = `50%`;
    })
    div.forEach(function(item) {
        item.onclick=function(){
            item.remove();
        }
    })
}


