/*Задача 1 
Напиши функцию map(fn, array), которая принимает на вход функцию и массив,
 и обрабатывает каждый элемент массива этой функцией, возвращая новый массив.
*/
document.write("Task 1" + '<br>');//вибираємо з масиву позитивні числа і додаємо їх в новий масив
const numbers = [56, 75, -8, 54, -84, 99, 0, -33, 21, 16, 111, -4];//задаємо масив
const positiv = [];//створюємо масив для позитивних елементів
const addpositiv = (elem) => {//робимо callback-функцію, що приймає аргумент а та додає його до масиву positiv
    positiv.unshift(elem);
}

const map = (array, callback) => {
    /*let a;*/
    for (let i = 0; i < array.length; i++) { //перебор всіх елементів масиву
        if (array[i] >= 0) {
            callback(array[i]); //визов callback-функції з аргументом і-го елементу масиву array
        }
    }
    document.write(positiv); //виводим на екран масив із позитивних чисел після всіх ітерацій
}
map(numbers, addpositiv);//передаємо масив numbes в array і функцію ddpositiv в callback-функцію

document.write('<hr/>');

/*Задача 2
Перепишите функцию, используя оператор '?' или '||'
Следующая функция возвращает true, если параметр age больше 18. 
В ином случае она задаёт вопрос confirm и возвращает его результат.
1 function checkAge(age) {
2 if (age > 18) {
3 return true;
4 } else {
5 return confirm('Родители разрешили?');
6 } }*/
document.write("Task 2" + '<br>');
function checkAge(age) {
    age > 18 ? true : window.confirm('Родители разрешили?');
};