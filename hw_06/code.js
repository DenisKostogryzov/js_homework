/*Створити клас Car , Engine та Driver.
Клас Driver містить поля - ПІБ, стаж водіння.
Клас Engine містить поля – потужність, виробник.
Клас Car містить поля – марка автомобіля, клас автомобіля, вага, водій типу Driver, мотор типу Engine.
 Методи start(), stop(), turnRight(), turnLeft(), які виводять на друк: "Поїхали", "Зупиняємося", "Поворот праворуч" 
 або "Поворот ліворуч". А також метод toString(), який виводить повну інформацію про автомобіль, її водія і двигуна.

Створити похідний від Car клас - Lorry (вантажівка), що характеризується також вантажопідйомністю кузова.
Створити похідний від Car клас - SportCar, який також характеризується граничною швидкістю*/
class Car {//Створюємо головний клас Car
    constructor(mark, carClass, weight, Driver, Engine) {//Властивості
        this.mark = mark;
        this.carClass = carClass;
        this.weight = weight;
        this.driver = Driver;//Передаємо у властивість інші класи
        this.engine = Engine;
    }
    start() {
        document.write("Поїхали");//Методи класу Car
    };
    stop() {
        document.write("Зупиняємося");
    };
    turnRight() {
        document.write("Поворот праворуч");
    };
    turnLeft() {
        document.write("Поворот ліворуч");
    };
    toString() {//Метод, що виводить всі властивості екземпляру
        document.write(this.mark + '<br/>', this.carClass + '<br/>', this.weight + '<br/>', this.driver.driversName + '<br/>', this.driver.stage + '<br/>', this.engine.power + '<br/>', this.engine.manufacturer + '<br/>');
    }
}
class Driver {
    constructor(driversName, stage) {
        this.driversName = driversName;
        this.stage = stage;
    }
}
class Engine {
    constructor(power, manufacturer) {
        this.power = power;
        this.manufacturer = manufacturer;
    }
}

class Lorry extends Car {
    constructor(mark, carClass, weight, Driver, Engin, loadCapacity) {
        super(mark, carClass, weight, Driver, Engin);
        this.loadCapacity = loadCapacity;
    }
}

class SportCar extends Car {
    constructor(mark, carClass, weight, Driver, Engin, topSpeed) {
        super(mark, carClass, weight, Driver, Engin);
        this.topSpeed = topSpeed;
    }
}

const engine1 = new Engine('285 hp', 'Cosworth');//Створення екземплярів класів
const driver1 = new Driver('Петренко Петро Петрович', '17 years');
const car1 = new SportCar('Lotus', 'coupe', '950 kg', driver1, engine1, '255');

console.log(car1);
car1.toString();
car1.start();

document.write('<hr/>');

const engine2 = new Engine('550 hp', 'Renault');//Створення екземплярів класів
const driver2 = new Driver('Сидоренко Сидор Сидорович', '23 years');
const car2 = new Lorry('Renault', 'truck', '20500 kg', driver2, engine2, '20000 kg');

console.log(car2);
car2.toString();
car2.stop();