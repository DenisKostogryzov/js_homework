//Task 5.1 Напишіть функцію isEmpty(obj), яка повертає true, якщо об'єкт не має властивостей, інакше false
const emptyObj = {};//створюємо об'єкт без властивостей
const obj2prop = { property1: 69, property2: 96 };//створюємо об'єкт з двома власивостями
const isEmpty = (obj) => { //створюємо функцію isEmpty, яка приймає об'єкт, визначає кількість його
    //властивостей та повертає true,  якщо об'єкт не має властивостей, інакше false*/
    if (Object.keys(obj).length == 0) {
        //       alert("Empty!");//у випадку "пустого" об'єкта викликає алерт Empty та повертає true. 
        return true;
    }
    else {
        //       alert("Not empty!");//у випадку об'єкта з властивостями викликає алерт Not empty! та повертає false. 
        return false;
    };
}
isEmpty(emptyObj);//викликаємо функцію isEmpty для об'єкта emptyObj
isEmpty(obj2prop);//викликаємо функцію isEmpty для об'єкта bj2prop

/*Task 5.2 Створити функцію-конструктор, що створює об'єкт Х'юмен. Створити масив об'єктів, та функцію що 
сортирує елементи масива по значенню властивості Age*/
//Створюємо масив і одразу записуємо все що потрібно.
//const arr = [new Object, new Array(), new Date()]
function Human(sex, age) {//конструктор 
    this.sex = sex;
    this.age = age;
}
/*const petro = new Human("male", 33);//створюємо об'єкти 
const vasil = new Human("male", 42);
const olha = new Human("female", 21);*/

const team = [new Human("male", 33), new Human("male", 42), new Human("female", 21)];//створюєумо масив з об'єктів
function ageSort(arr) {//підгледів цю фунуцію в інтернеті. До кінця не зрозумів, як вона в біса працює)))
    arr.sort((a, b) => a.age > b.age ? 1 : -1);
};
ageSort(team);//викликаємо фукцию на масиві team
console.log(team);//виводимо в консолі відсортований масив

/*Task 5.3 Створити функцію-конструктор, що створює об'єкт Х'юмен. Додати властивості і методи. 
Які з них треба зробити на рівні екземпляру, які на рівні функції-конструктора? */
Humanoid.countTo = 0;
function Humanoid(name, age, weight) {//конструктор
    this.name = name;
    this.age = age;
    this.weight = weight;
    this.greating = function () { //метод на рівні екземпляру
        document.write(`Сапієнс ${this.name} каже Привіт!`)};
    Humanoid.countTo++;//статична властивість
    Humanoid.warning = function () {//статичний метод
        if (Humanoid.countTo === 2) {
            document.write("</br>" + "Enough!");
        };
    };
};

Humanoid.makeRambo = function () {//метод на рівні функції-конструктор
    return new Humanoid("John", 75, 83);
};
Humanoid.makeRambo().greating();//
const balboa = new Humanoid("Rocky", 75, 83);
Humanoid.warning();